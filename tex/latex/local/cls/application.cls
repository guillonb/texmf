\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{cls/project}[2017/02/28]

\LoadClass[a4paper,UKenglish,french]{article}

\RequirePackage[tmargin=16ex,bmargin=16ex,lmargin=8em,rmargin=8em]{geometry}

\RequirePackage[font=small,textfont=sl]{caption}

\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
%\RequirePackage{lmodern}\normalfont
%\DeclareFontShape{T1}{lmr}{bx}{sc} { <-> ssub * cmr/bx/sc }{}
\RequirePackage{babel}
\RequirePackage{amsmath,amssymb}

\RequirePackage{xcolor}
\providecolor{darkgray}{rgb}{0.31,0.31,0.33}
\providecolor{lightgray}{rgb}{.51,.50,.52}
\providecolor{gbpurple}{RGB}{76,11,95}
\providecolor{gborange}{RGB}{255,188,51}

\RequirePackage[colorlinks,%
	urlcolor=gbpurple,% %eg, \href{http://…}
	linkcolor=black,% %eg, \ref{bla}
	citecolor=darkgray,% %eg, \cite{Gui16a}
]{hyperref}
%\RequirePackage{cleveref}							%french + lipics + cleveref →fail. Why?
\RequirePackage{thmtools}
\declaretheorem[name=Exemple,refname={exemple,exemples},Refname={Exemple,Exemples}]{example}

\RequirePackage[inline]{enumitem}
\setlist[itemize]{label=\raisebox{.25ex}{\textcolor{lightgray}{\rule{.75em}{3pt}}}}
\setlist[description]{font={\color{darkgray}\sffamily\bfseries\mathversion{bold}}}
\newcommand*\labelcdot{\textcolor{darkgray}{\large\ensuremath\cdot}}
\newcommand*\labelcdots{\textcolor{darkgray}{\large\ensuremath\cdots}}

%%%%%%%% additional fields
\def\keywords#1{\def\@keywords{#1}}
\let\@keywords\@empty

\def\subject#1{\def\@subject{#1}}
\let\@subject\@empty

\def\subtitle#1{\def\@subtitle{#1}}
\let\@subtitle\@empty

%%%%%%%% META-DATA
\RequirePackage{etoolbox}
\AfterEndPreamble{%
	\hypersetup{%
		pdfinfo={%
			Title={\@title\ifx\@subtitle\@empty\else\ -- \@subtitle\fi},%
			Author={\@author},%
			Keywords={\@keywords},%
			Subject={\@subject}%
		}%
	}%
}

%%%%%%%% MAKETITLE
\def\maketitle{%
	\begin{center}
		\huge%
		\textcolor{gborange}{\rule{.7\textwidth}{2pt}}\vspace{8pt}\linebreak
		\@title\linebreak
		\ifx\@subtitle\@empty\else\@subtitle\vspace{.5ex}\linebreak\fi
		\bfseries\mathversion{bold}\@author\vspace{-.5ex}\linebreak
		\textcolor{gborange}{\rule{.7\textwidth}{2pt}}\linebreak%
	\end{center}%
	\bigskip
}

%%%%%%%% SECTION
%\RequirePackage[titles]{tocloft}
\RequirePackage[explicit]{titlesec}
\RequirePackage{etoc}
\titleformat{\section}{\normalfont}{\colorbox{gborange}{\bfseries\Huge\thesection\quad\vphantom{fg}}}{0em}{%
	\bfseries\mathversion{bold}\Huge
	\hspace{.5em}%
	#1%
	\vspace{-2ex}%
	\newline\noindent%
	\textcolor{gborange}{\rule{.85\textwidth}{2pt}}%
}
\titleformat{\subsection}{\normalfont}%
{%
	\colorbox{gborange}{%
		\begin{minipage}{3em}%
			\bfseries\mathversion{bold}\large%
			\thesubsection%
			\vphantom{#1 $\mathcal F^f_i$}%
		\end{minipage}%
	}%
}%
{-1em}%
{%
	\colorbox{gborange}{%
		\begin{minipage}{\linewidth-3em}%
			\bfseries\mathversion{bold}\large%
			#1%
			\vphantom{\thesubsection $\mathcal F^f_i$}%
		\end{minipage}%
	}%
}
\titleformat{\subsubsection}{\normalfont}%
{\textcolor{darkgray}{\bfseries\large\thesubsubsection}}%
{1em}%
{%
	%\textcolor{gborange}{\rule{1em}{3pt}}
	%\textcolor{gborange}{\raisebox{-.1ex}{\rule{5pt}{5pt}}}%
	%\hspace{.25em}%
	\color{darkgray}\bfseries\mathversion{bold}\large\scshape
	#1%
	\hspace{.25em}%
	\textcolor{gborange}{\raisebox{-.1ex}{\rule{5pt}{5pt}}}%
}
\titleformat{\paragraph}{\normalfont}%
{}
{0em}
{%
	%\hsp[2em]%
	\bfseries\mathversion{bold}%
	\textcolor{gborange}{\rule{1em}{3pt}}
	#1
	\vsp[-1.25ex]
	%\hspace{.25em}\textcolor{gborange}{\raisebox{-.1ex}{\rule{1em}{3pt}}}%
}
\titleformat{\subparagraph}[runin]{\normalfont}%
{}
{0pt}
{%
	\vspace{-\baselineskip}%
	\newline\noindent%			% I did not found better than this hack.
	\bfseries\mathversion{bold}%
	\noindent #1
}

\newcommand*\namedlabel[3][]{%	%usage: \namedlabel[<ref name>]{<label name>}{<text>}
	\begingroup
	\ifx&#1&\def\@currentlabel{#3}\else\def\@currentlabel{#1}\fi%
	\phantomsection\label{#2}%
	#3%
	\endgroup
}

%%%%%%%%% AXIS
\titleclass{\axis}{straight}[\section]
\titleclass{\subaxis}{straight}[\axis]

\newcounter{axis}
\newcounter{subaxis}[axis]

\setcounter{secnumdepth}{5}

\newcommand\axisautoref{Axe}
\newcommand\subaxisautoref{Axe}

\titleformat{\axis}[hang]%
{\sffamily\Large\bfseries\mathversion{bold}}%format
{\raggedright\colorbox[rgb]{0.99,0.78,0.07}{AXE~\theaxis}}%label
{.5cm}%sep
{#1}%before-code
[]%after-code
\titlespacing*%
{\axis}%command
{\z@}
{3.25ex plus 1ex minus .2ex}%
{2.3ex plus .2ex}%

\titleformat{\subaxis}[hang]%
{\sffamily\large\bfseries\mathversion{bold}}%format
{\color{darkgray}(\theaxis.\thesubaxis)}%label
{.25cm}%sep
{#1\hspace{.25em}\textcolor{gborange}{\raisebox{-.1ex}{\rule{3pt}{3pt}}}}%before-code
[]%after-code
\titlespacing*{\subaxis}{\z@}%
{3.25ex plus 1ex minus .2ex}%
{1.5ex}%

\def\toclevel@axis{2}
\etocsetlevel{axis}{2}  
\def\l@axis{\@dottedtocline{2}{3.8em}{3.2em}}

\def\toclevel@subaxis{3}
\etocsetlevel{subaxis}{3}  
\def\l@subaxis{\@dottedtocline{3}{3.8em}{3.2em}}

%\newcommand\axis[1]{%
%	\stepcounter{axis}%
%	%
%	\@startsection{section}{2}{\z@}%
%	{-3.5ex \@plus -1ex \@minus -.2ex}%
%	{2.3ex \@plus .2ex}%
%	{\sffamily\Large\bfseries\raggedright\colorbox[rgb]{0.99,0.78,0.07}{AXE~\theaxis}\quad}*%
%	{#1}%
%	\addcontentsline{toc}{subsection}{\numberline{}AXE~\theaxis\ -- #1}%
%}
%\newcommand\subaxis[1]{%
%	\stepcounter{subaxis}%
%	\@startsection{section}{3}{\z@}%
%	{3.25ex \@plus 1ex \@minus .2ex}%
%	{1em}%
%	{\color{darkgray}\large\bfseries(\theaxis.\thesubaxis)~}*%
%	{%%as for subsubsections
%		\color{darkgray}\bfseries\mathversion{bold}\large\scshape #1%
%		\hspace{.25em}\textcolor{gborange}{\raisebox{-.1ex}{\rule{3pt}{3pt}}}%
%	}%
%	\addcontentsline{toc}{subsubsection}{\numberline{}(\theaxis.\thesubaxis) -- #1}%
%}

%%%%%%%%% QUESTIONS/PERSPECTIVE/OBJECTIFS
\newenvironment{questions}[1][questions]{%
	\bigbreak\par\noindent%
%	\textcolor{gborange}{\rule{1em}{1ex}}
	\textbf{\MakeUppercase #1}
	\textcolor{gborange}{\rule{2em}{1ex}}%
	\par\nobreak\@afterheading%
	\smallskip%
	\begin{itemize}[label=\textcolor{darkgray}{$\blacktriangleright$}]%
			}{
	\end{itemize}
}

%%%%%%%% TOC
%\setcounter{tocdepth}{4}

%%%%%%%% BIBLABEL
\def\@biblabel#1{\begin{minipage}{4.25em}\hfill\textcolor{darkgray}{{\sffamily\bfseries#1}}\end{minipage}}

%%%%%%%%% ADDITIONAL COMMANDS
\newcommand*\td{\textsc{td}}
\newcommand*\tp{\textsc{tp}}
\newcommand*\dm{\textsc{dm}}
\newcommand*\textsmall[1]{{\small #1}}
