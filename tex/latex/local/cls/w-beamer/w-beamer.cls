%INFO:  use prefix 'wb@' in private macros
%MODULE OPTIONS:  [no]actionkeeper
%OTHER OPTIONS:
%API:  \Action: argument processor (in the sense of xparse) that action the argument if the passed action is not -NoValue-
%MODULE API:  even when not loaded, commands defined by modules are defined
%TODO:  Find a nice way no avoid defining commands from an unloaded module
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{w-beamer}[2022/09/26 Beamer wrapper]

%DEPENDENCIES
\LoadClassWithOptions{beamer}
\RequirePackage{pgfopts}
\RequirePackage{xparse}

%IFS
\newif\if@wb@opt@module@actionkeeper@\@wb@opt@module@actionkeeper@true

%OPTIONS
\pgfkeys{
	/w-beamer/.cd,
	%
	actionkeeper/.is if=@wb@opt@module@actionkeeper@,
	actionkeeper/.default=true,
	%
}
\ProcessPgfOptions{/w-beamer}


%ARGUMENT PROCESSORS
\def\wb@argumentprocessor@identity#1{\gdef\ProcessedArgument{#1}}
\def\Action<#1>{\IfValueTF{#1}{\wb@argumentprocessor@ActionWithAction<#1>}{\wb@argumentprocessor@identity}}
\def\wb@argumentprocessor@ActionWithAction<#1>#2{\gdef\ProcessedArgument{\action<#1>{#2}}}


%%%%%%%%%%%%%%%%%%%%%%
\if@wb@opt@module@actionkeeper@
	%MODULE:  action keeper
	%INFO:  use prefix 'wb@actionkeeper@' in private macros
	%API:  \KeepAction: argument processor (in the sense of xparse) that keep action active in the argument
	%			 
	%CHANGES:  redefined \alert, \uncover, \only, \visible, \invisible, and \action beamer commands, so that action is kept within their arguments
	%TODO:  for-loop to redefine these commands in a shorter code
	\ClassInfo{w-beamer}{loading module actionkeeper}
	\def\wb@actionkeeper@inaction{*}
	\begingroup
		\catcode`@=12
		\expandafter\gdef\csname wb@actionkeeper@arobase\endcsname{@}
	\endgroup

	\let\wb@actionkeeper@old@alert\alert%
	\DeclareDocumentCommand\alert{D<>{*} m}{%
		\wb@actionkeeper@old@alert<#1>{%
			\edef\wb@actionkeeper@inaction{\ifx&\wb@actionkeeper@inaction&\else\wb@actionkeeper@inaction|\fi alert\wb@actionkeeper@arobase#1}#2%
		}%
	}
	\let\wb@actionkeeper@old@uncover\uncover%
	\DeclareDocumentCommand\uncover{D<>{*} m}{%
		\wb@actionkeeper@old@uncover<#1>{%
			\edef\wb@actionkeeper@inaction{\ifx&\wb@actionkeeper@inaction&\else\wb@actionkeeper@inaction|\fi uncover\wb@actionkeeper@arobase#1}#2%
		}%
	}
	\let\wb@actionkeeper@old@only\only%
	\DeclareDocumentCommand\only{D<>{*} m}{%
		\wb@actionkeeper@old@only<#1>{%
			\edef\wb@actionkeeper@inaction{\ifx&\wb@actionkeeper@inaction&\else\wb@actionkeeper@inaction|\fi only\wb@actionkeeper@arobase#1}#2%
		}%
	}
	\let\wb@actionkeeper@old@visible\visible%
	\DeclareDocumentCommand\visible{D<>{*} m}{%
		\wb@actionkeeper@old@visible<#1>{%
			\edef\wb@actionkeeper@inaction{\ifx&\wb@actionkeeper@inaction&\else\wb@actionkeeper@inaction|\fi visible\wb@actionkeeper@arobase#1}#2%
		}%
	}
	\let\wb@actionkeeper@old@invisible\invisible%
	\DeclareDocumentCommand\invisible{D<>{*} m}{%
		\wb@actionkeeper@old@invisible<#1>{%
			\edef\wb@actionkeeper@inaction{\ifx&\wb@actionkeeper@inaction&\else\wb@actionkeeper@inaction|\fi invisible\wb@actionkeeper@arobase#1}#2%
		}%
	}
	\let\wb@actionkeeper@old@action\action%
	\DeclareDocumentCommand\action{D<>{*} m}{%
		\wb@actionkeeper@old@action<#1>{%
			\edef\wb@actionkeeper@inaction{\ifx&\wb@actionkeeper@inaction&\else\wb@actionkeeper@inaction|\fi#1}#2%
		}%
	}

	\def\keepaction#1{\action<\wb@actionkeeper@inaction>{#1}}%the macro
	\def\KeepAction#1{\gdef\ProcessedArgument{\keepaction{#1}}}%the argument processor
\else
	\ClassInfo{w-beamer}{not loading module actionkeeper}
	\def\keepaction{}
	\let\KeepAction\wb@argumentprocessor@identity
\fi
