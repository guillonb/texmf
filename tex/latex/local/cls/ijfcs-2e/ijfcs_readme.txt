International Journal of Foundations of Computer Science

LaTeX2e Style Guide

World Scientific has produced a style/template document in LaTeX2e format for authors contributing to International Journal of Foundations of Computer Science (IJFCS).

The World Scientific LaTeX2e class file and the sample coded file are located on the website in zip format called ijfcs-2e.zip. Download and unzip this. Once unzipped successfully, you will find the following files:

ws-ijfcs.cls	:	LaTeX2e class file
ws-ijfcs.tex	:	sample LaTeX coded file for Typesetting Manuscripts
ws-ijfcs.pdf	:	sample Typeset pages
ijfcsf1.eps	:	Figure/Image file for LaTeX users
ijfcsf1.pdf	:	Figure/Image file for PDFLaTeX users
ws-ijfcs.bst	:	Bibliography style file for BiBTeX users
sample.bib	:	Sample BiBTeX database
ws-ijfcs_bib.tex	:	sample file coded in BiBTeX
ws-ijfcs_bib.pdf	:	PDF output of ws-ijfcs_bib.tex

Click here to download the above files.

For help with the use of the World Scientific LaTeX2e class file, please e-mail to either:
ykoh@wspc.com.sg or rajesh@wspc.com.sg 